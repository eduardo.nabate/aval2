package cest.edu.aval2;

/**
 * Hello world
 *
 
 */
import java.math.BigDecimal;

import cest.edu.aval2.rh.Contador;
import cest.edu.aval2.rh.Fornecedor;
import cest.edu.aval2.rh.Funcionario;
import cest.edu.aval2.rh.PessoaFisica;
import cest.edu.aval2.rh.PessoaJuridica;
//
public class App {
	/*
	 * public static void main( String[] args ) { new App().run(); }
	 * 
	 * public void run() { System.out.println( "Hello World!" ); }
	 */
	public static void main(String[] args) {
		// PF
		PessoaFisica pessoaA = new PessoaFisica("diego", "159");
		System.out.println(pessoaA.getNome());
		///// pessoaA.setNome("joão");
		// PJ
		PessoaJuridica pj = new PessoaJuridica("patrao", "55.564.414/0001-35");
		System.out.println(pj.getCNPJ());

		//// PessoaFisica teste = new PessoaFisica("antonio", "789789");
		// (teste.endereco.setLogradouro("Urcine alvez"), "maike", "5");
		// (endeco.[teste.endereco.logradouro], "maike", "5");
		///// teste.endereco.cidade.uf.setSigla("MG");

		// Funcionario
		// nome cpf,cargo de um funcionario
		Funcionario novato = new Funcionario("Luiz algusto", "32132165-478", "VENDEDOR", null);
		novato.setNome("joaquim");
		System.out.println(novato.getCargo());
		// salario de um funcionario
		Funcionario func = new Funcionario("teste", "12", "dev", null);
		BigDecimal salario1 = new BigDecimal("5000.00");
		func.setSalario(salario1);
		System.out.println(func.getSalario());

		// Contador
		Contador cont = new Contador();
		cont.setProxNumero(50);
		System.out.println(cont.getProxNumero());

		// Fornecedor
		Fornecedor fornecedor = new Fornecedor("LG", "49.976.987/0001-73");
		System.out.println(fornecedor.getCNPJ());
		// teste endereco !!!
		//// System.out.println(pessoaA.getEndereco().getCidade().getSigla());

		/*// cliente
		// Cliente cli = new Cliente(2021/05/05, 1);
		Cliente cli = new Cliente();
		cli.setNumero(5);
		System.out.println(cli.getNumero());*/

	}

}
