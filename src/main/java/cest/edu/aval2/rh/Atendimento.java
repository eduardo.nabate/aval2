package cest.edu.aval2.rh;

import java.util.Date;

public class Atendimento {
	// atributos
	protected Date data;
	protected int numero;

	// construtor
	public Atendimento(Date data, int numero) {
		super();
		this.data = data;
		this.numero = numero;
	}

	
	//getter setter
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void finalizar() {
		System.out.println("finalizando atendimento");
	}
}