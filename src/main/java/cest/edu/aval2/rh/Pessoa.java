package cest.edu.aval2.rh;

public class Pessoa {
	// atributos
	protected Endereco endereco;
	protected String nome;
	protected String cpf;

	// construtor
	public Pessoa(Endereco endereco, String nome, String cpf) {
		this.endereco = endereco;
		this.nome = nome;
		this.cpf = cpf;
	}

	// contrutor 2
	public Pessoa(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}

	// construtor 3
	public Pessoa(Endereco endereco, String nome) {
		this.nome = nome;
		this.endereco = endereco;
	}

	// construtor 4
	public Pessoa(String nome) {
		this.nome = nome;
	}

	// getter e setter
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}
}
