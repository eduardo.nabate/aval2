package cest.edu.aval2.rh;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Cliente extends Atendimento { // foi visto que não pode estar sendo extendindo de Atendimento deveria ser
											// uma interface 05/12/2021

	// atributos
	protected Atendimento[] atendimentosFechados;

	// construtor
	public Cliente(Date data, int numero) {
		super(data, numero);
	}
	


	List<Atendimento> atendimentos = new ArrayList<Atendimento>();

	// getter setter
	public List<Atendimento> getAtendimentos() {
		return atendimentos;
	}

	public void setAtendimentos(List<Atendimento> atendimentos) {
		this.atendimentos = atendimentos;
	}

	public Atendimento[] getAtendimentosFechados() {
		return atendimentosFechados;
	}

	public void setAtendimentosFechados(Atendimento[] atendimentosFechados) {
		this.atendimentosFechados = atendimentosFechados;
	}

	// metodos

}