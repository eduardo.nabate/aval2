package cest.edu.aval2.rh;

public class Uf {
	// atributos
	protected String sigla;
	protected String descricao;

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public String getDescricao() {
		return descricao;
	}
}
