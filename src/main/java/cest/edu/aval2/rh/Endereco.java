package cest.edu.aval2.rh;

public class Endereco extends Cidade {
	// atributos
	protected Cidade cidade;
	protected String logradouro;

	// eduardo
	public Endereco(Cidade cidade, String logradouro) {
		this.cidade = cidade;
		this.logradouro = logradouro;
	}
	// fim

	// get set
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	// outros metodos
}
