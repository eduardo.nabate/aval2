package cest.edu.aval2.rh;

public class Contador {

	// atributos
	private Contador Instance;
	private int proxNumero;

	// construtor
	public Contador() {
	}

	// getter setters
	public Contador getInstance() {
		return Instance;
	}

	public void setInstance(Contador instance) {
		Instance = instance;
	}

	public int getProxNumero() {
		return proxNumero;
	}

	public void setProxNumero(int proxNumero) {
		this.proxNumero = proxNumero;
	}

}
