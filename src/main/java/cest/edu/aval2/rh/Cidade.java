package cest.edu.aval2.rh;

public class Cidade extends Uf {
	// atributos
	protected String nome;
	protected Uf uf;

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getNome() {
		return nome;
	}

	public Uf getUf() {
		return uf;
	}
}
