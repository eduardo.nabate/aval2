package cest.edu.aval2.rh;

import java.math.BigDecimal;

public class Funcionario extends PessoaFisica {
	// atributos
	protected String cargo;
	protected BigDecimal salario;

	// construtor1
	public Funcionario(String nome, String cpf, String cargo, BigDecimal salario) {
		super(nome, cpf);
		this.cargo = cargo;
		this.salario = salario;
	}

	// construtor2
	/*public Funcionario(String nome) {
		this.nome = nome;
	}*/

	// get set
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

	public String getCargo() {
		return cargo;
	}

	public BigDecimal getSalario() {
		return salario;
	}
	// outros metodos
}
